Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: SCM
Source: http://people.csail.mit.edu/jaffer/SCM.html

Files: *.* ANNOUNCE ChangeLog inc2scm Makefile QUICKREF README configure
Copyright: 1984-2014 by the Free Software Foundation
           1996, 2000-2001, 2003, 2007, 2008 by Aubrey Jaffer
           1992-1997 by Tanel Tammet
           1989 by Paradigm Associates Inc.
           1985-1991 by Digital Equipment Corp.
           1990-1991 by Tektronix, Inc.
License: LGPL-3+
 This program is free software: you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License as published
 by the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this program.  If not, see
 `http://www.gnu.org/licenses/'.
 .
 On Debian systems, the complete text of the GNU Lesser General Public License
 version 2 can be found in "/usr/share/common-licenses/LGPL-3".

Files: unexelf.c unexmacosx.c unexsgi.c unexsunos4.c gmalloc.c lastfile.c r4rstest.scm syntest2.scm unexalpha.c unexec.c build build.scm ecrt0.c findexec.c bench.scm
Copyright: 1984-2014 by the Free Software Foundation
           1996, 2000-2001, 2003, 2007, 2008 by Aubrey Jaffer
           1990 by W. Wilson Ho
           1992 by William Clinger
License: GPL-3+
 This program is free software: you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License as published
 by the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this program.  If not, see
 `http://www.gnu.org/licenses/'.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-3".

Files: scm.texi scm.info
Copyright: 2007 Free Software Foundation, Inc. <http://fsf.org/>
License: GFDL-1.3+
 Permission is granted to copy, distribute and/or modify this
 document under the terms of the GNU Free Documentation License,
 Version 1.3 or any later version published by the Free Software
 Foundation; with no Invariant Sections, no Front-Cover Texts, and
 no Back-Cover Texts.  A copy of the license is included in the
 section entitled "GNU Free Documentation License."
 .
 On Debian systems, the complete text of the GFDL version 1.3 can be found in
 "/usr/share/common-licenses/GFDL-1.3".

Files: debian/*
Copyright: 2017 by Bryan Newbold
	   2021 by Barak A. Pearlmutter
License: permissive
 Copying and distribution of this package, with or without modification, are
 permitted in any medium without royalty provided the copyright notice and this
 notice are preserved.
